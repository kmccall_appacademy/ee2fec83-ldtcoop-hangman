class Hangman
  def initialize(options = { guesser: HumanPlayer.new, referee: ComputerPlayer.new })
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    @board = "_" * @referee.pick_secret_word
    @guesser.register_secret_length(@board.length)
    @hangings = 1
  end

  attr_reader :guesser, :referee, :board, :hangings

  # Leland Krych's animation solution from Piazza
  HANGMAN_HASH = {
    1 =>["
       +---+
       |   |
           |
           |
           |
           |
    ========="],
    2 => ["
      +---+
      |   |
      O   |
          |
          |
          |
    ========="],
    3 => ["
      +---+
      |   |
      O   |
      |   |
          |
          |
    ========="],
    4 => ["
      +---+
      |   |
      O   |
     /|   |
          |
          |
    ========="],
    5 => ["
      +---+
      |   |
      O   |
     /|\\  |
          |
          |
    ========="],
    6 => ["
      +---+
      |   |
      O   |
     /|\\  |
     /    |
          |
    ========="],
   7 => ["
      +---+
      |   |
      O   |
     /|\\  |
     / \\  |
          |
    ========="]
  }.freeze

  def take_turn
    guess = @guesser.guess(@board)
    guess_arr = @referee.check_guess(guess)
    if !guess_arr.empty?
      update_board(guess, guess_arr)
    else
      @hangings += 1
    end
    @guesser.handle_response(guess, guess_arr)
  end

  def self.guess_play
    game = Hangman.new
    game.setup
    loop do
      puts HANGMAN_HASH[game.hangings]
      puts "Guessed Letters: #{game.guesser.guesses}"
      puts game.board
      game.take_turn
      if !game.board.include?("_")
        puts "You won!"
        break
      elsif game.hangings == 7
        puts "RIP! You lost!"
        puts "The word was '#{game.referee.secret_word}'"
        puts HANGMAN_HASH[7]
        break
      end
    end
  end

  def self.ref_play
    game = Hangman.new(guesser: ComputerPlayer.new, referee: HumanPlayer.new)
    game.setup
    loop do
      puts HANGMAN_HASH[game.hangings]
      puts game.board
      game.take_turn
      if !game.board.include?("_")
        puts "The computer got it!"
        break
      elsif game.hangings == 7
        puts "You won!"
        puts "RIP computer."
        puts HANGMAN_HASH[7]
        break
      end
    end
  end

  private

  def update_board(letter, guess_arr)
    guess_arr.each do |el|
      @board[el] = letter
    end
  end
end

class HumanPlayer
  def initialize
    @guesses = ""
  end

  attr_reader :guesses

  #begin referee methods

  def pick_secret_word
    loop do
      puts "How many letters are in your word?"
      len = gets.chomp.to_i
      return len if len > 0
      puts "That's not a valid length!"
    end
  end

  def check_guess(_guess)
    puts "Enter the indicies where that letter appears"
    puts "Or just press enter if it doesn't appear"
    indicies = gets.chomp.split(",").map(&:to_i)
    indicies
  end
  #begin guesser methods

  def guess(_board)
    puts "What is your guess?"
    loop do
      guess = gets.chomp.downcase
      if guess.length == 1
        @guesses += guess
        return guess
      end
      puts "That's not a letter!"
    end
  end

  def register_secret_length(length)
    @secret_length = length
  end

  def handle_response(let, guess_arr); end

  #needs to be here to keep Computer and Human APIs the same
  def candidate_words; end
end

class ComputerPlayer
  def initialize(dict = dictionary_import)
    @dict = dict
    @candidate_words = dict
  end

  attr_reader :dict, :secret_word, :candidate_words, :secret_length

  # begin referee methods
  def pick_secret_word
    @secret_word = @dict[rand_gen]
    @secret_word.length
  end

  def check_guess(letter)
    (0...@secret_word.length).find_all do |i|
      @secret_word[i, 1] == letter
    end
  end

  # end referee methods
  # begin guesser methods

  def guess(board)
    self.register_secret_length(board.length)
    letter = frequency_finder(board)
    puts "Is the letter '#{letter}' in your word?"
    letter
  end

  def register_secret_length(len)
    @candidate_words = @candidate_words.select do |word|
      word.length == len
    end
  end

  def handle_response(let, guess_arr)
    if guess_arr.empty?
      @candidate_words = @candidate_words.reject do |word|
        word.include?(let)
      end
    else
      # selects only words with let in guess_arr positions
      guess_arr.each do |idx|
        @candidate_words = @candidate_words.select do |word|
          word[idx] == let && word.count(let) == guess_arr.length
        end
      end
    end
  end

  private

  def rand_gen
    rand(0...@dict.length)
  end

  def dictionary_import
    # error handling to make sure the dictionary can be found
    begin
      File.read("lib/dictionary.txt").split("\n")
    rescue
      File.read("dictionary.txt").split("\n")
    end
  end

  def frequency_finder(board)
    freqs = Hash.new(0)
    # loops through an array to look at each word
    @candidate_words.each do |word|
      # loops through each word to find letter frequencies
      word.each_char { |char| freqs[char] += 1 unless board.include?(char) }
    end
    max_record = freqs.max_by(&:last)
    max_record && max_record.first
  end
end

def game_start
  loop do
    puts "Would you like to be a (G)uesser or a (R)eferee?"
    input = gets.chomp
    if input.upcase == "G"
      Hangman::guess_play
      break
    elsif input.upcase == "R"
      Hangman::ref_play
      break
    else
      puts "I'm sorry, what was that?"
    end
  end
end


play_game = true
while play_game
  game_start
  puts "Would you like to play again?"
  input = gets.chomp.upcase
  if input == "Y"
    next
  else
    play_game = false
  end
end
